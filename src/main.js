
import Vue from 'vue'
import VueRouter from 'vue-router'
import DashboardPlugin from './material-dashboard'
import VueSelect from 'vue-select'
import axios from 'axios'
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import Notifications from 'vue-notification'

// Plugins
import App from './App.vue'
import Chartist from 'chartist'

// router setup
import routes from './routes/routes'

axios.defaults.baseURL = 'https://latovus-dev.herokuapp.com';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.interceptors.request.use(
  conf => {
    conf.headers.common['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5ZTkwY2U2MWQyZDUyMWZmYzdjMWZiMiIsImlhdCI6MTUzNjQyMzYzN30.HMVTAV1QpN-zYztt4URpkCv502bdft3bClA_1veXSYo';
    return conf;
  },
  error => {
    return Promise.reject(error);
  }
);



Vue.component('v-select', VueSelect.VueSelect);
// plugin setup
Vue.use(VueRouter)
Vue.use(DashboardPlugin)
Vue.use(VueFormWizard)
Vue.use(Notifications)

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkExactActiveClass: 'nav-item active'
});

// global library setup
Object.defineProperty(Vue.prototype, '$Chartist', {
  get () {
    return this.$root.Chartist
  }
});


/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  data: {
    Chartist: Chartist
  }
});
