import axios from 'axios';
// file-upload.service.js
const uploadFile = async (file, token) => {
  const BASE_URL = axios.defaults.baseURL;
  let response = {
    errMsg: null,
    uploadedFile: null
  };
  const data = new FormData();
  data.append('file', file, 'ticket-image');

  try {
    const stream = await fetch(`${BASE_URL}/uploads`, {
      headers: new Headers({
        'Authorization': `Bearer ${token}`,
      }),
      method: 'post',
      body: data
    });
    if (!stream.ok) {
      switch (stream.status) {
        case 401:
          response.errMsg = 'non autorizzato';
          break;
        case 400:
          response.errMsg = 'alcune informazioni non sono corrette';
          break;
        case 404:
          response.errMsg = 'utente non trovato';
          break;
        default:
          response.errMsg = 'errore';
          break;
      }
    }
    response.uploadedFile = await stream.json();
  }
  catch (e) {
    response.errMsg = e.message;
  }

  return Promise.resolve(response);

};

export { uploadFile }
